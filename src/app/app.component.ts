import { Component, OnInit } from '@angular/core';
import {
  ConfiguratorProject
} from '@sieval/hub-client';
import { HubService } from 'src/sieval-hub/hub.service';
import { config } from 'src/config';

@Component({
  selector: 'sds-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private readonly KEY_PROJECT = 'shoppingCartProject';
  
  constructor(
    //private hubService: HubService
  ) {

  }
  
  title = 'demo-fonq';
  
  project: ConfiguratorProject | null = null;

  async ngOnInit() {
    try {
      const project = localStorage.getItem(this.KEY_PROJECT);
      if (project) {
        this.project = JSON.parse(project);
      }

      //await this.hubService.getToken(config.emailAddress, config.password).toPromise();

    } catch (err) {
      console.error(err);
    }
  }
}
