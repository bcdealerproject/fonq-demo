import { Component, OnInit } from '@angular/core';
import { HubService } from '@sieval-hub/hub.service';
import {
  ConfiguratorProject,
  Customer,
  HubApplication,
  InputProjectBuilder,
  Material,
  MaterialBrowseRequest,
  Polygon
} from '@sieval/hub-client';
import { config } from 'src/config';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private hubService: HubService) { }

  applications: HubApplication[] = [];

  materials: Material[] = [];

  async ngOnInit() {
    this.applications = await this.hubService.getUserApplications().toPromise();

    const request: MaterialBrowseRequest = {
      filter: {
        categoryId: config.categoryId,
        pageSize: 5,
        pageIndex: 0
      }
    };

    const response = await this.hubService
        .browseMaterials(request, this.applications[0].applicationId)
        .toPromise();

    this.materials = response.items;
  }

}
