import '@sieval/configurator';

import { Component, ElementRef, OnInit, ViewChild, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguratorProject } from '@sieval/hub-client';
import { config } from 'src/config';
import { ConfiguratorConfig } from 'src/interfaces';
import { ConfiguratorLayout } from 'src/interfaces/configurator-layout';
import * as $ from 'jquery';

@Component({
  selector: 'sds-configurator-page',
  templateUrl: './configurator-page.component.html',
  styleUrls: ['./configurator-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConfiguratorComponent implements OnInit {
  private readonly KEY_PROJECT = 'shoppingCartProject';
  public width: number;
  public height: number;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {}

  @ViewChild('configurator', { read: ElementRef })
  configuratorElement: ElementRef;

  configuratorConfig: ConfiguratorConfig;

  apiKey = config.apiKey;

  ngOnInit() {
    
    const materialId = null;
    const materialCode = this.route.snapshot.paramMap.get('materialCode');
    const width = parseInt(this.route.snapshot.paramMap.get('width'));
    const height = parseInt(this.route.snapshot.paramMap.get('height'));
    const polygonId = this.route.snapshot.paramMap.get('polygonId');

    this.width = width;
    this.height = height;

    const configuratorConfig: ConfiguratorConfig = {
      startWithDefaultModel: true,
      defaultWidth: width,
      defaultHeight: height,
      useProduction: false,
      settings: {
        // change this to select a layout
        layout: ConfiguratorLayout.Basic
      }
    };

    const project = localStorage.getItem(this.KEY_PROJECT);
    if (project) {
      configuratorConfig.project = JSON.parse(project);
    }

    if (polygonId) {
      configuratorConfig.polygonId = polygonId;
    } else {
      configuratorConfig.material = { materialId, materialCode };
    }

    this.configuratorConfig = configuratorConfig;

    const element = this.configuratorElement.nativeElement as HTMLElement;
    (<any>element).config = configuratorConfig;
    element.addEventListener('addToCart', (event: CustomEvent) => this.onAddToCart(event.detail));

    setInterval(function() {
      if($('.configurator-price').length > 0) {
        var elementPrice = $('.configurator-price-before-promotion')[0];
        var price = $(elementPrice).html();
        
        if($.isNumeric(price)) {
          price = price.replace('.', ',');
          if($('#amount').html() != price) {
            $('#amount').html(price);
          }
        }
      }
    }, 500);
    
  }

  onAddToCart(project: ConfiguratorProject) {
    if($('#confirm-msg').length <= 0) {
      var btn = $('.configurator-add-to-cart')[0];
      $("body").append("<div id='confirm-msg'>Toegevoegd aan winkelwagen</div>");
      $('#confirm-msg').css("left", $(btn).position().left + 165 + "px");
      $('#confirm-msg').css("top", $(btn).position().top - 50 + "px");
      
      setTimeout(function() {
        $('#confirm-msg').fadeOut("slow", function() {
          $('#confirm-msg').remove();
        });
      }, 5000);
    }
    //const json = JSON.stringify(project);
    //localStorage.setItem(this.KEY_PROJECT, json);
    //this.router.navigate(['/']);
  }

}