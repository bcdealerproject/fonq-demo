import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HubService} from 'src/sieval-hub/hub.service';

import { AppComponent } from './app.component';
import { ConfiguratorComponent } from './configurator-page/configurator-page.component';
import { ProductListComponent } from './product-list/product-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfiguratorComponent,
    ProductListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: ProductListComponent},
      {path: 'configurator/:materialCode/:width/:height', component: ConfiguratorComponent},
      {path: '**', component: AppComponent},
    ],
    //{ enableTracing: true }
    { useHash: true }
    )
  ],
  providers: [HubService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
